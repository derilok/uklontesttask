﻿namespace Uklon.Models
{
    using System;
    
    public class Region
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public DateTime LastUpdated { get; set; }
    }
}
