﻿namespace Uklon.Models
{
    using System.ComponentModel.DataAnnotations;
    
    public class TrafficJam
    {
        [Key]
        public int RegionId { get; set; }
        public int TrafficLevel { get; set; }
        public string Timestamp { get; set; }
    }
}
