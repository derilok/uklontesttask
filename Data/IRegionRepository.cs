﻿namespace Uklon.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Uklon.Models;

    public interface IRegionRepository
    {
        IQueryable<Region> Regions { get; }

        Task SaveRegionsAsync(IEnumerable<Region> regions);
    }
}
