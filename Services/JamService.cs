﻿namespace Uklon.Services
{
    using System;
    using System.IO;
    using System.Net.Http;
    using System.Threading.Tasks;
    using System.Xml.Serialization;
    using Microsoft.Extensions.Configuration;
    using Uklon.Models;
    using Uklon.Services.XmlModels;

    public class JamService
    {
        private IConfiguration configuration;
        private readonly Random rnd = new Random();

        public JamService(IConfiguration configuration)
        {
            this.configuration = configuration;
        }

        public async Task<TrafficJam> GetDataByRegionAsync(int region)
        {
            var useFakeData = Convert.ToBoolean(this.configuration["JamServiceProviders:UseFake"]);
            if (useFakeData)
            {
                return FakeTrafficFor(region);
            }

            using (var httpClient = new HttpClient())
            {
                var baseUrl = new Uri(this.configuration["JamServiceProviders:Url"]);
                var path = new Uri(baseUrl, $"?region={region}&bustCache={DateTimeOffset.UtcNow.ToUnixTimeSeconds()}");
                var serializer = new XmlSerializer(typeof(Info));
                try
                {
                    var response = await httpClient.GetStringAsync(path);
                    if (string.IsNullOrEmpty(response))
                    {
                        return FakeTrafficFor(region);
                    }
                    var xmlObject = (Info)serializer.Deserialize(new StringReader(response));
                    return new TrafficJam
                    {
                        RegionId = Convert.ToInt32(xmlObject.Region.Id),
                        TrafficLevel = Convert.ToInt32(xmlObject.Traffic.Region?.Level ?? "0"),
                        Timestamp = xmlObject.Traffic.Region?.Timestamp ?? DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()
                    };
                }
                catch (HttpRequestException _e)
                {
                    return FakeTrafficFor(region);
                }
            }
        }

        private TrafficJam FakeTrafficFor(int region)
        {
            return new TrafficJam
            {
                RegionId = region,
                TrafficLevel = this.rnd.Next(0, 11),
                Timestamp = DateTimeOffset.UtcNow.ToUnixTimeSeconds().ToString()
            };
        }
    }
}
