﻿namespace Uklon
{
    using System.IO;
    using System.Threading;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Sheets.v4;
    using Google.Apis.Util.Store;
    using Microsoft.AspNetCore.Builder;
    using Microsoft.AspNetCore.Hosting;
    using Microsoft.AspNetCore.Mvc;
    using Microsoft.EntityFrameworkCore;
    using Microsoft.Extensions.Configuration;
    using Microsoft.Extensions.DependencyInjection;
    using Uklon.Data;
    using Uklon.Services;

    public class Startup
    {
        public Startup(IConfiguration configuration)
        {
            Configuration = configuration;
            AddGoogleCredentials();
        }

        public IConfiguration Configuration { get; }
        public UserCredential GoogleCredential { get; private set; }

        // This method gets called by the runtime. Use this method to add services to the container.
        public void ConfigureServices(IServiceCollection services)
        {
            services.AddMvc().SetCompatibilityVersion(CompatibilityVersion.Version_2_1);
            services.AddTransient<SheetService>();
            services.AddSingleton(this.GoogleCredential);
            services.AddDbContext<ApplicationDbContext>(options => options.UseSqlite(this.Configuration["Data:ConnectionString"]));
            services.AddTransient<IRegionRepository, RegionRepository>();
            services.AddTransient<ITrafficJamRepository, TrafficJamRepository>();
            services.AddTransient<JamService>();
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }
            else
            {
                app.UseHsts();
            }

            app.UseHttpsRedirection();
            app.UseMvc();
        }

        private void AddGoogleCredentials()
        {
            string[] scopes = { SheetsService.Scope.SpreadsheetsReadonly };
            using (var stream = new FileStream("google_creds.json", FileMode.Open, FileAccess.Read))
            {
                string credPath = "token.json";
                this.GoogleCredential = GoogleWebAuthorizationBroker.AuthorizeAsync(
                    GoogleClientSecrets.Load(stream).Secrets,
                    scopes,
                    "user",
                    CancellationToken.None,
                    new FileDataStore(credPath, true)).Result;
            }

        }
    }
}
