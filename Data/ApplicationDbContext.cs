﻿namespace Uklon.Data
{
    using System;
    using Microsoft.EntityFrameworkCore;
    using Uklon.Models;

    public class ApplicationDbContext : DbContext
    {
        public ApplicationDbContext(DbContextOptions<ApplicationDbContext> options)
            : base(options)
        {
        }

        public DbSet<Region> Regions { get; set; }
        public DbSet<TrafficJam> TrafficJams { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.Entity<Region>().Property<DateTime>("LastUpdated").HasDefaultValue(new DateTime(2000, 1, 1));
        }
    }
}
