﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace Uklon.Migrations
{
    public partial class AddTrafficJams : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "TrafficJams",
                columns: table => new
                {
                    RegionId = table.Column<int>(nullable: false)
                        .Annotation("Sqlite:Autoincrement", true),
                    TrafficLevel = table.Column<int>(nullable: false),
                    Timestamp = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_TrafficJams", x => x.RegionId);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "TrafficJams");
        }
    }
}
