﻿namespace Uklon.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Uklon.Models;

    public class TrafficJamRepository : ITrafficJamRepository
    {
        private readonly ApplicationDbContext context;

        public TrafficJamRepository(ApplicationDbContext ctx)
        {
            this.context = ctx;
        }

        public IQueryable<TrafficJam> TrafficJams => this.context.TrafficJams;

        public async Task SaveTrafficJamAsync(TrafficJam jam)
        {
            if (this.TrafficJams.Any(tj => tj.RegionId == jam.RegionId))
            {
                this.context.Update(jam);
            }
            else
            {
                await this.context.AddAsync(jam);
            }
        }

        public async Task SaveTrafficJamsAsync(IEnumerable<TrafficJam> jams)
        {
            foreach (var jam in jams)
            {
                await this.SaveTrafficJamAsync(jam);
            }
        }
    }
}
