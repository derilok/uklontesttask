# Available API calls:
    * GET /api/jams -> all traffic by all regions
    * GET /api/jams/:regions_id -> traffic by specific region
    * GET /api/jams/regions -> all available regions