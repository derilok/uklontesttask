﻿namespace Uklon.Services.XmlModels
{
    using System.Xml.Serialization;
    using System.Collections.Generic;

    [XmlRoot(ElementName = "region")]
    public class Region
    {
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlAttribute(AttributeName = "zoom")]
        public string Zoom { get; set; }
        [XmlAttribute(AttributeName = "lat")]
        public string Lat { get; set; }
        [XmlAttribute(AttributeName = "lon")]
        public string Lon { get; set; }
        [XmlElement(ElementName = "length")]
        public string Length { get; set; }
        [XmlElement(ElementName = "level")]
        public string Level { get; set; }
        [XmlElement(ElementName = "icon")]
        public string Icon { get; set; }
        [XmlElement(ElementName = "timestamp")]
        public string Timestamp { get; set; }
        [XmlElement(ElementName = "time")]
        public string Time { get; set; }
        [XmlElement(ElementName = "hint")]
        public List<Hint> Hint { get; set; }
        [XmlElement(ElementName = "tend")]
        public string Tend { get; set; }
        [XmlElement(ElementName = "url")]
        public string Url { get; set; }
    }

    [XmlRoot(ElementName = "hint")]
    public class Hint
    {
        [XmlAttribute(AttributeName = "lang")]
        public string Lang { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "traffic")]
    public class Traffic
    {
        [XmlElement(ElementName = "region")]
        public Region Region { get; set; }
        [XmlAttribute(AttributeName = "region")]
        public string _Region { get; set; }
        [XmlElement(ElementName = "title")]
        public string Title { get; set; }
        [XmlAttribute(AttributeName = "zoom")]
        public string Zoom { get; set; }
        [XmlAttribute(AttributeName = "lat")]
        public string Lat { get; set; }
        [XmlAttribute(AttributeName = "lon")]
        public string Lon { get; set; }
    }

    [XmlRoot(ElementName = "day")]
    public class Day
    {
        [XmlAttribute(AttributeName = "weekday")]
        public string Weekday { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "month")]
    public class Month
    {
        [XmlAttribute(AttributeName = "name")]
        public string Name { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "date")]
    public class Date
    {
        [XmlElement(ElementName = "day")]
        public Day Day { get; set; }
        [XmlElement(ElementName = "month")]
        public Month Month { get; set; }
        [XmlElement(ElementName = "year")]
        public string Year { get; set; }
        [XmlElement(ElementName = "daytime")]
        public string Daytime { get; set; }
        [XmlAttribute(AttributeName = "date")]
        public string _date { get; set; }
    }

    [XmlRoot(ElementName = "image-v2")]
    public class Imagev2
    {
        [XmlAttribute(AttributeName = "size")]
        public string Size { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "image-v3")]
    public class Imagev3
    {
        [XmlAttribute(AttributeName = "size")]
        public string Size { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "wind_direction")]
    public class Wind_direction
    {
        [XmlAttribute(AttributeName = "id")]
        public string Id { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "temperature")]
    public class Temperature
    {
        [XmlAttribute(AttributeName = "class_name")]
        public string Class_name { get; set; }
        [XmlAttribute(AttributeName = "color")]
        public string Color { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "day_part")]
    public class Day_part
    {
        [XmlElement(ElementName = "weather_type")]
        public string Weather_type { get; set; }
        [XmlElement(ElementName = "weather_code")]
        public string Weather_code { get; set; }
        [XmlElement(ElementName = "image")]
        public string Image { get; set; }
        [XmlElement(ElementName = "image-v2")]
        public Imagev2 Imagev2 { get; set; }
        [XmlElement(ElementName = "image-v3")]
        public Imagev3 Imagev3 { get; set; }
        [XmlElement(ElementName = "image_number")]
        public string Image_number { get; set; }
        [XmlElement(ElementName = "wind_speed")]
        public string Wind_speed { get; set; }
        [XmlElement(ElementName = "wind_direction")]
        public Wind_direction Wind_direction { get; set; }
        [XmlElement(ElementName = "dampness")]
        public string Dampness { get; set; }
        [XmlElement(ElementName = "hectopascal")]
        public string Hectopascal { get; set; }
        [XmlElement(ElementName = "torr")]
        public string Torr { get; set; }
        [XmlElement(ElementName = "pressure")]
        public string Pressure { get; set; }
        [XmlElement(ElementName = "temperature")]
        public Temperature Temperature { get; set; }
        [XmlElement(ElementName = "time_zone")]
        public string Time_zone { get; set; }
        [XmlElement(ElementName = "observation_time")]
        public string Observation_time { get; set; }
        [XmlElement(ElementName = "observation")]
        public string Observation { get; set; }
        [XmlAttribute(AttributeName = "typeid")]
        public string Typeid { get; set; }
        [XmlAttribute(AttributeName = "type")]
        public string Type { get; set; }
        [XmlElement(ElementName = "temperature_from")]
        public Temperature_from Temperature_from { get; set; }
        [XmlElement(ElementName = "temperature_to")]
        public Temperature_to Temperature_to { get; set; }
    }

    [XmlRoot(ElementName = "temperature_from")]
    public class Temperature_from
    {
        [XmlAttribute(AttributeName = "class_name")]
        public string Class_name { get; set; }
        [XmlAttribute(AttributeName = "color")]
        public string Color { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "temperature_to")]
    public class Temperature_to
    {
        [XmlAttribute(AttributeName = "class_name")]
        public string Class_name { get; set; }
        [XmlAttribute(AttributeName = "color")]
        public string Color { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "night_short")]
    public class Night_short
    {
        [XmlElement(ElementName = "temperature")]
        public Temperature Temperature { get; set; }
    }

    [XmlRoot(ElementName = "tomorrow")]
    public class Tomorrow
    {
        [XmlElement(ElementName = "temperature")]
        public Temperature Temperature { get; set; }
    }

    [XmlRoot(ElementName = "url")]
    public class Url
    {
        [XmlAttribute(AttributeName = "slug")]
        public string Slug { get; set; }
        [XmlText]
        public string Text { get; set; }
    }

    [XmlRoot(ElementName = "weather")]
    public class Weather
    {
        [XmlElement(ElementName = "source")]
        public string Source { get; set; }
        [XmlElement(ElementName = "day")]
        public Day Day { get; set; }
        [XmlElement(ElementName = "url")]
        public Url Url { get; set; }
        [XmlAttribute(AttributeName = "climate")]
        public string Climate { get; set; }
        [XmlAttribute(AttributeName = "region")]
        public string Region { get; set; }
    }

    [XmlRoot(ElementName = "info")]
    public class Info
    {
        [XmlElement(ElementName = "region")]
        public Region Region { get; set; }
        [XmlElement(ElementName = "traffic")]
        public Traffic Traffic { get; set; }
        [XmlElement(ElementName = "weather")]
        public Weather Weather { get; set; }
        [XmlAttribute(AttributeName = "lang")]
        public string Lang { get; set; }
    }

}
