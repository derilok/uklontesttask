﻿namespace Uklon.Controllers
{
    using System;
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Microsoft.AspNetCore.Mvc;
    using Uklon.Data;
    using Uklon.Models;
    using Uklon.Services;

    [Route("api/[controller]")]
    public class JamsController : Controller
    {
        private SheetService sheet;
        private JamService jam;
        private IRegionRepository regionRepository;
        private ITrafficJamRepository trafficJamRepository;

        public JamsController(SheetService sheet, JamService jam, IRegionRepository regionRepository, ITrafficJamRepository trafficJamRepository)
        {
            this.sheet = sheet;
            this.jam = jam;
            this.regionRepository = regionRepository;
            this.trafficJamRepository = trafficJamRepository;
        }

        // GET: api/jams/regions
        [HttpGet("Regions")]
        public async Task<IEnumerable<Region>> RegionsAsync()
        {
            IEnumerable<Region> result;
            if (this.regionRepository.Regions.Count() > 0 && regionRepository.Regions.Any(region => region.LastUpdated > DateTime.Now.AddMinutes(-1)))
            {
                result = regionRepository.Regions.ToList();
            }
            else
            {
                result = this.sheet.GetRegions();
                await regionRepository.SaveRegionsAsync(result);
            }
            return result;
        }

        // GET: api/jams
        [HttpGet]
        public async Task<IEnumerable<TrafficJam>> Get()
        {
            var regions = await this.RegionsAsync();
            var result = new List<TrafficJam>();
            foreach (var region in regions)
            {
                result.Add(await this.Get(region.Id));
            }
            return result;
        }

        // GET api/jams/:id
        [HttpGet("{id}")]
        public async Task<TrafficJam> Get(int id)
        {
            TrafficJam result;
            if (this.trafficJamRepository.TrafficJams.Any(jam => jam.RegionId == id))
            {
                var tj = this.trafficJamRepository.TrafficJams.First(jam => jam.RegionId == id);
                var updatedAt = DateTimeOffset.FromUnixTimeMilliseconds(Convert.ToInt64(tj.Timestamp));
                if (updatedAt > DateTime.Now.AddMinutes(-1))
                {
                    result = tj;
                }
                else
                {
                    result = await this.jam.GetDataByRegionAsync(id);
                    await this.trafficJamRepository.SaveTrafficJamAsync(result);
                }
            }
            else
            {
                result = await this.jam.GetDataByRegionAsync(id);
            }
            return result;
        }
    }
}
