﻿namespace Uklon.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Uklon.Models;

    public class RegionRepository : IRegionRepository
    {
        private readonly ApplicationDbContext context;

        public RegionRepository(ApplicationDbContext ctx)
        {
            this.context = ctx;
        }

        public IQueryable<Region> Regions => this.context.Regions;

        public async Task SaveRegionsAsync(IEnumerable<Region> regions)
        {
            foreach (var region in regions)
            {
                if (this.Regions.Any(r => r.Id == region.Id))
                {
                    this.context.Update(region);
                }
                else
                {
                    await this.context.AddAsync(region);
                }
            }
            this.context.SaveChanges();
        }
    }
}
