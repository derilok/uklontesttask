﻿namespace Uklon.Data
{
    using System.Collections.Generic;
    using System.Linq;
    using System.Threading.Tasks;
    using Uklon.Models;

    public interface ITrafficJamRepository
    {
        IQueryable<TrafficJam> TrafficJams { get; }

        Task SaveTrafficJamAsync(TrafficJam jam);
        Task SaveTrafficJamsAsync(IEnumerable<TrafficJam> jams);
    }
}
