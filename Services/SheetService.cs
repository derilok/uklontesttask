﻿namespace Uklon.Services
{
    using System;
    using System.Collections.Generic;
    using Google.Apis.Auth.OAuth2;
    using Google.Apis.Services;
    using Google.Apis.Sheets.v4;
    using Uklon.Infrastructure;
    using Uklon.Models;

    public class SheetService
    {
        private UserCredential credential;

        public SheetService(UserCredential credential)
        {
            this.credential = credential;
        }

        public IEnumerable<Region> GetRegions()
        {
            var service = new SheetsService(new BaseClientService.Initializer()
            {
                HttpClientInitializer = credential,
                ApplicationName = "Traffic Jams",
            });
            var spreadsheetId = "1AhemqFP2lZ4ifcXGmArOydA3w24Yd7LdQ3KZveN-JR4";
            var range = "Регионы!A2:B";
            var request = service.Spreadsheets.Values.Get(spreadsheetId, range);
            var response = request.Execute();
            var values = response.Values;
            IList<Region> regions = new List<Region>();
            if (values != null && values.Count > 0)
            {
                foreach (var row in values)
                {
                    if (Int32.TryParse((string)row[0], out int id))
                    {
                        regions.Add(new Region { Id = id, Name = (string)row[1], LastUpdated = DateTime.Now });
                    }
                }
            }
            return regions.DistinctBy(r => r.Id);
        }
    }
}
